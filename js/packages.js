/**
 * IE6 detection and upgrade notice
 * Copyright Soak Ltd 2011
 */
var ua=navigator.userAgent.toLowerCase();var client={isIE:ua.indexOf("msie")>-1,isIE6:ua.indexOf("msie 6")>-1};if(client.isIE&&client.isIE6){addLoadEvent(display_warning);}function addLoadEvent(a){var b=window.onload;if(typeof window.onload!="function"){window.onload=a}else{window.onload=function(){a();if(b){b()}}}}function display_warning(){var c=document.body.innerHTML;var a="text-decoration: underline; color: #0D394D; font-weight:bold;";var e="";e+="<style> html, body { overflow-y: hidden!important; height: 100%; </style>";e+="<div style='position: absolute; top: 0; left: 0; margin: 0; height: 17px; padding: 6px 5px 3px 15px; font-family: Verdana, Helvetica, Arial, sans-serif; font-size:10px; background-color:#FFFFE1; color: #333; border-bottom: 1px solid #A7A6AA;'>";e+="<div style='float:right; text-align:right; width:60px; margin: auto 5px;'>";e+="</div>";e+="You are using an out-of-date and unsupported version of Internet Explorer, <a style='"+a+"' target='_blank' href='http://www.microsoft.com/windows/downloads/ie/getitnow.mspx'>click here</a> to download the latest version. Fancy a better browser? Try <a style='"+a+"' target='_blank' href='http://www.mozilla.com/firefox/'>Mozilla Firefox</a> or <a style='"+a+"' target='_blank' href='http://www.google.com/chrome/'>Google Chrome</a>.";e+="</div>";var d="";d+="<div style='height:25px; line-height:25px; font-size:10px; display:block; margin:0px; padding:0px;'>";d+="</div>";var b="";b+="<div style='width:100%; margin:0px; padding:0px; height:100%; overflow-y: scroll; position:relative;'>";b+=d;b+=c;b+="</div>";document.body.innerHTML=b+e}


function updatefigures(id,startObj,endObj)
{
	var ajaxStart = encodeURIComponent(startObj.val());
	var ajaxEnd = encodeURIComponent(endObj.val());
	if((startObj.val() != '' && endObj.val() != '') && (startObj.val() != endObj.val())) {
		$.ajax({
			url: '/api/booking-cost?vehicleLocation=' + id + '&startAt=' + ajaxStart + '&endAt=' + ajaxEnd,
			dataType: 'json',
			success: function(data) {

				if(data.result.insurancePremium == '0.00')
				{
					$('#hirecost').html('&pound;' + data.result.basePrice);
					$('#insurancecost').html('N/A');
					$('#bookingcost').html('&pound; ' + data.result.bookingFee);
					$('#rental-costs #totalcost').html('&pound;' + data.result.total);
					if(!$('.form-errors').length)
					{
						var jsTel = $('#bookingVehicleId').attr('data-tel');
						$('section.booking-form').prepend('<ul class="form-errors"><li>Unfortunately we are unable to provide you with an insurance quotation right now. Please contact HiyaCar on ' + jsTel + ' to discuss this.</li></ul>')
					}
					return false;
				}

				$('#hirecost').html('&pound;' + data.result.basePrice);
				$('#insurancecost').html('&pound;' + data.result.insurancePremium);
				$('#bookingcost').html('&pound; ' + data.result.bookingFee);
				$('#rental-costs #totalcost').html('&pound;' + data.result.total);
			}
		});
	}
}

function populate(frmObj, frmDate, frmHr, frmMin, cal, plusone)
{

	var now = new Date();
	var newDate = new Date(now.getFullYear(),now.getMonth(),now.getDate(),now.getHours()+1,now.getMinutes(),now.getSeconds());

	if(!frmHr) frmHr = newDate.getHours();
	if(!frmMin) frmMin = '00';
	if(!frmDate) {
		frmDate = newDate;
	}

	dateDD = frmDate.getDate();
	dateMM = frmDate.getMonth()+1; //January is 0!
	dateYYYY = frmDate.getFullYear();

	if(plusone && frmHr < 24)
	{
		frmHr = parseInt(frmHr) + 1;
		if(frmHr < 10)
		{
			frmHr = '0' + frmHr;
		}
		
	}

	var realDate = new Date();
	realDate.setFullYear(dateYYYY, dateMM - 1, dateDD);
	if(plusone && frmHr == 24)
	{
		frmHr = '00';
		realDate.setTime(realDate.getTime() + 86400000);
	}
	cal.datepicker( "setDate" , realDate );
	frmObj.siblings('div').find('.hour').val(frmHr);
	frmObj.siblings('div').find('.min').val(frmMin);

	newDateDD = realDate.getDate();
	newDateMM = (parseInt(realDate.getMonth())+1); //January is 0!
	newDateYYYY = realDate.getFullYear();

	frmObj.val(('0' + realDate.getDate()).slice(-2) + '/' + ('0' + (realDate.getMonth()+1)).slice(-2) + '/' + realDate.getFullYear() + ' ' + frmHr + ':' + frmMin);
}

// check end date is not before start date
function checkDates(startObj,endObj)
{
	// current format dd/mm/yyyy HH:MM
	var startTimeNodes = startObj.val().split(/[\s:\/]+/);
	var startTime = new Date(startTimeNodes[2], startTimeNodes[1], startTimeNodes[0], startTimeNodes[3], startTimeNodes[4]);
	var endTimeNodes = endObj.val().split(/[\s:\/]+/);
	var endTime = new Date(endTimeNodes[2], endTimeNodes[1], endTimeNodes[0], endTimeNodes[3], endTimeNodes[4]);

	if(startTime >= endTime)
	{
		return false;
	}
	return true;
}

function prePopulate(dateVal,overlayParent,cal,calId)
{

	if(!dateVal)
	{
		$('#' + calId).val('');
	}

	var datetime = $.trim(dateVal).split(' ');
	var datePart = datetime[0];
	var timePart = datetime[1];
	if(timePart)
	{
		var times = timePart.split(':');
		overlayParent.find('.hour').val(times[0]);
		overlayParent.find('.min').val(times[1]);
		cal.datepicker( "setDate" , datePart );
	}

	if(dateVal)
	{
		$('#' + calId).val(dateVal);

	}

}

function handleDateRangePickers(startObj, endObj, vehicleId)
{

	var vehicleId = $('#bookingVehicleId').attr('data-rel');

	var ajax = $('#rental-costs').length;

	if(ajax && endObj.val() && startObj.val())
	{
		updatefigures($('#booking-ids').val(),startObj,endObj);	
	}

	//grab start and End Id
	var startId = startObj.attr('id');
	var endId = endObj.attr('id');

	var startVal = $('#' + startId).val();
	var endVal = $('#' + endId).val();

	// Generate unique instance of Start and End Overlay
	startObj.after('<div id="startOverlay' + startId + '" class="datetimepicker"><div id="startCal' + startId + '"></div><form class="times"><label for="frm-hour-' + startId + '">Time:</label><select class="hour" name="frm-hour-' + startId + '" id="frm-hour-' + startId + '"><option value="">--</option><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select><label class="w3c" for="frm-min-' + startId + '">Min:</label> <span>:</span> <select class="min" name="frm-min-' + startId + '" id="frm-min-' + startId + '"><option value="">--</option><option value="00">00</option><option value="15">15</option><option value="30">30</option><option value="45">45</option></select><p class="cal-actions"><input type="image" src="/static/images/btn.png" alt="Confirm" class="button cal-confirm" /><input type="image" src="/static/images/btn.png" alt="Cancel" class="button cal-cancel" /></p></form><p class="availableTimes"></p></div>')
	endObj.after('<div id="endOverlay' + endId + '" class="datetimepicker"><div id="endCal' + endId + '"></div><form class="times"><label for="frm-hour-' + endId + '">Time:</label><select class="hour" name="frm-hour-' + endId + '" id="frm-hour-' + endId + '"><option value="">--</option><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select><label class="w3c" for="frm-min-' + endId + '">Min:</label> <span>:</span> <select class="min" name="frm-min-' + endId + '" id="frm-min-' + endId + '"><option value="">--</option><option value="00">00</option><option value="15">15</option><option value="30">30</option><option value="45">45</option></select><p class="cal-actions"><input type="image" src="/static/images/btn.png" alt="Confirm" class="button cal-confirm" /><input type="image" src="/static/images/btn.png" alt="Cancel" class="button cal-cancel" /></p></form><p class="availableTimes"></p></div>')

	// define unique Start and End Cal
	var startObjCal = $('#startCal' + startId);
	var endObjCal = $('#endCal' + endId);

	// define unique Start and End Cal
	var startObjCalOverlay = $('#startOverlay' + startId);	
	var endObjCalOverlay = $('#endOverlay' + endId);	


	// trigger Show
	startObj.live('click',function() {
		$('.datetimepicker').hide();
		startObjCalOverlay.show();
	});

	// trigger Show
	endObj.live('click',function() {
		$('.datetimepicker').hide();
		endObjCalOverlay.show();
	});


	// bind Unique Close button
	startObjCalOverlay.on('click','.cal-cancel',function(){
		var start = startObjCal.datepicker('getDate');
		populate(startObj,start,$('#frm-hour-'+startId).val(),$('#frm-min-'+startId).val(),startObjCal,false);
		populate(endObj,start,$('#frm-hour-'+startId).val(),$('#frm-min-'+startId).val(),endObjCal,true);
		startObjCalOverlay.hide();
		startObj.trigger('change');

		// update prices
		if(ajax) updatefigures($('#booking-ids').val(),startObj,endObj);			

		return false;
	});

	// bind Unique Close button
	endObjCalOverlay.on('click','.cal-cancel',function(){
		if(checkDates(startObj,endObj))
		{
			var end = endObjCal.datepicker('getDate');
			populate(endObj,end,$('#frm-hour-'+endId).val(),$('#frm-min-'+endId).val(),endObjCal,false);
			endObjCalOverlay.hide();

			// update prices
			if(ajax) updatefigures($('#booking-ids').val(),startObj,endObj);
		}
		else
		{
			var start = startObjCal.datepicker('getDate');
			populate(endObj,start,$('#frm-hour-'+startId).val(),$('#frm-min-'+startId).val(),endObjCal,true);
			alert("Notice: Your booking Drop off date/time cannot be before your Pick up date/time.\nWe have reset your Drop off time.");
		}
		endObj.trigger('change');
		return false;
	});

	// bind Unique Confirm button
	startObjCalOverlay.on('click','.cal-confirm',function(){

		var start = startObjCal.datepicker('getDate');
		populate(startObj,start,$('#frm-hour-'+startId).val(),$('#frm-min-'+startId).val(),startObjCal,false);
		populate(endObj,start,$('#frm-hour-'+startId).val(),$('#frm-min-'+startId).val(),endObjCal,true);
		startObjCalOverlay.hide();
		startObj.trigger('change');

		// update prices
		if(ajax) updatefigures($('#booking-ids').val(),startObj,endObj);			

		return false;
	});

	// bind Unique Confirm button
	endObjCalOverlay.on('click','.cal-confirm',function(){

		if(checkDates(startObj,endObj))
		{
			var end = endObjCal.datepicker('getDate');
			populate(endObj,end,$('#frm-hour-'+endId).val(),$('#frm-min-'+endId).val(),endObjCal,false);
			endObjCalOverlay.hide();

			// update prices
			if(ajax) updatefigures($('#booking-ids').val(),startObj,endObj);
		}
		else
		{
			var start = startObjCal.datepicker('getDate');
			populate(endObj,start,$('#frm-hour-'+startId).val(),$('#frm-min-'+startId).val(),endObjCal,true);
			alert("Notice: Your booking Drop off date/time cannot be before your Pick up date/time.\nWe have reset your Drop off time.");
		}

		endObj.trigger('change');
		return false;
	});


	// bind Unique Select Changes
	startObjCalOverlay.on('change','#frm-hour-'+startId+',#frm-min-'+startId,function(){
		var start = startObjCal.datepicker('getDate');
		populate(startObj,start,$('#frm-hour-'+startId).val(),$('#frm-min-'+startId).val(),startObjCal,false);
		populate(endObj,start,$('#frm-hour-'+startId).val(),$('#frm-min-'+startId).val(),endObjCal,true);
		return false;
	});

	// bind Unique Select Changes
	endObjCalOverlay.on('change','#frm-hour-'+endId+',#frm-min-'+endId,function(){
		var end = endObjCal.datepicker('getDate');
		populate(endObj,end,$('#frm-hour-'+endId).val(),$('#frm-min-'+endId).val(),endObjCal,false);
		return false;
	});


	$myBadDates = new Array()
	if(vehicleId)
	{
		var startDate = new Date();
		var buildStart = startDate.getDay() + '/' + startDate.getMonth() + '/' + startDate.getFullYear();
		var buildEnd = startDate.getDay() + '/' + startDate.getMonth() + '/' + (parseInt(startDate.getFullYear()) +1);
		$.getJSON('/api/unavailable-dates?vehicleLocation=' + vehicleId + '&startDate=' + buildStart + '&endDate=' + buildEnd, function(data) {
			
			$myBadDates = data.result;

		});

	}
	else
	{
		var $myBadDates = new Array();
	}

	startObjCal.datepicker({
		onSelect: function (selectedDate, inst){
			var start = $(this).datepicker('getDate');
			endObj.datepicker('option', 'minDate', new Date(start.getTime()));
			populate(startObj,start,$('#frm-hour-'+startId).val(),$('#frm-min-'+startId).val(),startObjCal,false);
			// Set End date to be 1 hour in the future
			populate(endObj,start,$('#frm-hour-'+startId).val(),$('#frm-min-'+startId).val(),endObjCal,true);
			endObjCal.datepicker('option', 'minDate', new Date(start.getTime()));
		},
		altField:'#'+startId,
		minDate: new Date(),
		dateFormat: "dd/mm/yy",
		altFormat: "dd/mm/yy",
        beforeShowDay: function(mydate) {
			var $return=true;
			var $returnclass ="available";
			$checkdate = $.datepicker.formatDate('dd/mm/yy', mydate);
			for(var i = 0; i < $myBadDates.length; i++)
				{    
				   if($myBadDates[i] == $checkdate)
					  {
					$return = false;
					$returnclass= "unavailable";
					}
				}
			return [$return,$returnclass];	
		}
	});

	endObjCal.datepicker({
		onSelect: function (selectedDate, inst){
				var end = $(this).datepicker('getDate');
				populate(endObj,end,$('#frm-hour-'+endId).val(),$('#frm-min-'+endId).val(),endObjCal,false);
		},
		altField:'#'+endId,
		minDate: new Date(),
		dateFormat: "dd/mm/yy",
		altFormat: "dd/mm/yy",
		beforeShowDay: function(mydate) {
			var $return=true;
			var $returnclass ="available";
			$checkdate = $.datepicker.formatDate('dd/mm/yy', mydate);
			for(var i = 0; i < $myBadDates.length; i++)
				{    
				   if($myBadDates[i] == $checkdate)
					  {
					$return = false;
					$returnclass= "unavailable";
					}
				}
			return [$return,$returnclass];	
		}
	});	

	
	startObjCalOverlay.attr('data-rel',startId);
	endObjCalOverlay.attr('data-rel',endId);

	prePopulate(startVal, startObjCalOverlay, startObjCal, startId);
	prePopulate(endVal, endObjCalOverlay, endObjCal, endId);

	if(startId == 'frm-vehicle-booking-start-at') bindCalendarAvailableActions(vehicleId);
	if(startId == 'frm-booking-start-at') bindCalendarAvailableActions(vehicleId);
}


// Function used to display if a date is available, if so returns availble times
// Called on Hover of datepicker table cell

function bindCalendarAvailableActions(vehicleId) {

	if($('[data-rel="frm-vehicle-booking-start-at"]').length)
	{
		var filterStart = '[data-rel="frm-vehicle-booking-start-at"]';
		var filterEnd = '[data-rel="frm-vehicle-booking-end-at"]';
	}
	if($('[data-rel="frm-booking-start-at"]').length)
	{
		var filterStart = '[data-rel="frm-booking-start-at"]';
		var filterEnd = '[data-rel="frm-booking-end-at"]';
	}

	$(filterStart + ' td.available:not(.ui-state-disabled)').live({
	  mouseenter: function(){
		
		var selectedDate = new Date($(this).text() + '/' + $(filterStart + ' .ui-datepicker-month').text() + '/' + $(filterStart + ' .ui-datepicker-year').text());
		var buildDate = $(this).text() + '/' + (selectedDate.getMonth() +1) + '/' + selectedDate.getFullYear();

		$.getJSON('/api/available-times?vehicleLocation=' + vehicleId + '&date=' + buildDate, function(data) {
			$('.availableTimes').text(data.result);
		});

		$('.availableTimes').show();
		
	  },
	  mouseleave: function(){
		$('.availableTimes').hide();
	  }
	});

	$(filterEnd + ' td.available:not(.ui-state-disabled)').live({
	  mouseenter: function(){
		
		var selectedDate = new Date($(this).text() + '/' + $(filterEnd + ' .ui-datepicker-month').text() + '/' + $(filterEnd + ' .ui-datepicker-year').text());
		var buildDate = $(this).text() + '/' + (selectedDate.getMonth() +1) + '/' + selectedDate.getFullYear();

		$.getJSON('/api/available-times?vehicleLocation=' + vehicleId + '&date=' + buildDate, function(data) {
			$('.availableTimes').text(data.result);
		});

		$('.availableTimes').show();
		
	  },
	  mouseleave: function(){
		$('.availableTimes').hide();
	  }
	});
	
}





function ordi(n){
var s='th';
if(n===1 || n==21 || n==31) s='st';
if(n===2 || n==22) s='nd';
if(n===3 || n==23) s='rd';
return n+s;
}


/**
 * jQuery lightBox plugin
 * This jQuery plugin was inspired and based on Lightbox 2 by Lokesh Dhakar (http://www.huddletogether.com/projects/lightbox2/)
 * and adapted to me for use like a plugin from jQuery.
 * @name jquery-lightbox-0.5.js
 * @author Leandro Vieira Pinho - http://leandrovieira.com
 * @version 0.5
 * @date April 11, 2008
 * @category jQuery plugin
 * @copyright (c) 2008 Leandro Vieira Pinho (leandrovieira.com)
 * @license CCAttribution-ShareAlike 2.5 Brazil - http://creativecommons.org/licenses/by-sa/2.5/br/deed.en_US
 * @example Visit http://leandrovieira.com/projects/jquery/lightbox/ for more informations about this jQuery plugin
 */
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('(6($){$.2N.3g=6(4){4=23.2H({2B:\'#34\',2g:0.8,1d:F,1M:\'18/5-33-Y.16\',1v:\'18/5-1u-2Q.16\',1E:\'18/5-1u-2L.16\',1W:\'18/5-1u-2I.16\',19:\'18/5-2F.16\',1f:10,2A:3d,2s:\'1j\',2o:\'32\',2j:\'c\',2f:\'p\',2d:\'n\',h:[],9:0},4);f I=N;6 20(){1X(N,I);u F}6 1X(1e,I){$(\'1U, 1S, 1R\').l({\'1Q\':\'2E\'});1O();4.h.B=0;4.9=0;7(I.B==1){4.h.1J(v 1m(1e.17(\'J\'),1e.17(\'2v\')))}j{36(f i=0;i<I.B;i++){4.h.1J(v 1m(I[i].17(\'J\'),I[i].17(\'2v\')))}}2n(4.h[4.9][0]!=1e.17(\'J\')){4.9++}D()}6 1O(){$(\'m\').31(\'<e g="q-13"></e><e g="q-5"><e g="5-s-b-w"><e g="5-s-b"><1w g="5-b"><e 2V="" g="5-k"><a J="#" g="5-k-V"></a><a J="#" g="5-k-X"></a></e><e g="5-Y"><a J="#" g="5-Y-29"><1w W="\'+4.1M+\'"></a></e></e></e><e g="5-s-b-T-w"><e g="5-s-b-T"><e g="5-b-A"><1i g="5-b-A-1t"></1i><1i g="5-b-A-1g"></1i></e><e g="5-1s"><a J="#" g="5-1s-22"><1w W="\'+4.1W+\'"></a></e></e></e></e>\');f z=1D();$(\'#q-13\').l({2K:4.2B,2J:4.2g,S:z[0],P:z[1]}).1V();f R=1p();$(\'#q-5\').l({1T:R[1]+(z[3]/10),1c:R[0]}).E();$(\'#q-13,#q-5\').C(6(){1a()});$(\'#5-Y-29,#5-1s-22\').C(6(){1a();u F});$(G).2G(6(){f z=1D();$(\'#q-13\').l({S:z[0],P:z[1]});f R=1p();$(\'#q-5\').l({1T:R[1]+(z[3]/10),1c:R[0]})})}6 D(){$(\'#5-Y\').E();7(4.1d){$(\'#5-b,#5-s-b-T-w,#5-b-A-1g\').1b()}j{$(\'#5-b,#5-k,#5-k-V,#5-k-X,#5-s-b-T-w,#5-b-A-1g\').1b()}f Q=v 1j();Q.1P=6(){$(\'#5-b\').2D(\'W\',4.h[4.9][0]);1N(Q.S,Q.P);Q.1P=6(){}};Q.W=4.h[4.9][0]};6 1N(1o,1r){f 1L=$(\'#5-s-b-w\').S();f 1K=$(\'#5-s-b-w\').P();f 1n=(1o+(4.1f*2));f 1y=(1r+(4.1f*2));f 1I=1L-1n;f 2z=1K-1y;$(\'#5-s-b-w\').3f({S:1n,P:1y},4.2A,6(){2y()});7((1I==0)&&(2z==0)){7($.3e.3c){1H(3b)}j{1H(3a)}}$(\'#5-s-b-T-w\').l({S:1o});$(\'#5-k-V,#5-k-X\').l({P:1r+(4.1f*2)})};6 2y(){$(\'#5-Y\').1b();$(\'#5-b\').1V(6(){2u();2t()});2r()};6 2u(){$(\'#5-s-b-T-w\').38(\'35\');$(\'#5-b-A-1t\').1b();7(4.h[4.9][1]){$(\'#5-b-A-1t\').2p(4.h[4.9][1]).E()}7(4.h.B>1){$(\'#5-b-A-1g\').2p(4.2s+\' \'+(4.9+1)+\' \'+4.2o+\' \'+4.h.B).E()}}6 2t(){$(\'#5-k\').E();$(\'#5-k-V,#5-k-X\').l({\'K\':\'1C M(\'+4.19+\') L-O\'});7(4.9!=0){7(4.1d){$(\'#5-k-V\').l({\'K\':\'M(\'+4.1v+\') 1c 15% L-O\'}).11().1k(\'C\',6(){4.9=4.9-1;D();u F})}j{$(\'#5-k-V\').11().2m(6(){$(N).l({\'K\':\'M(\'+4.1v+\') 1c 15% L-O\'})},6(){$(N).l({\'K\':\'1C M(\'+4.19+\') L-O\'})}).E().1k(\'C\',6(){4.9=4.9-1;D();u F})}}7(4.9!=(4.h.B-1)){7(4.1d){$(\'#5-k-X\').l({\'K\':\'M(\'+4.1E+\') 2l 15% L-O\'}).11().1k(\'C\',6(){4.9=4.9+1;D();u F})}j{$(\'#5-k-X\').11().2m(6(){$(N).l({\'K\':\'M(\'+4.1E+\') 2l 15% L-O\'})},6(){$(N).l({\'K\':\'1C M(\'+4.19+\') L-O\'})}).E().1k(\'C\',6(){4.9=4.9+1;D();u F})}}2k()}6 2k(){$(d).30(6(12){2i(12)})}6 1G(){$(d).11()}6 2i(12){7(12==2h){U=2Z.2e;1x=27}j{U=12.2e;1x=12.2Y}14=2X.2W(U).2U();7((14==4.2j)||(14==\'x\')||(U==1x)){1a()}7((14==4.2f)||(U==37)){7(4.9!=0){4.9=4.9-1;D();1G()}}7((14==4.2d)||(U==39)){7(4.9!=(4.h.B-1)){4.9=4.9+1;D();1G()}}}6 2r(){7((4.h.B-1)>4.9){2c=v 1j();2c.W=4.h[4.9+1][0]}7(4.9>0){2b=v 1j();2b.W=4.h[4.9-1][0]}}6 1a(){$(\'#q-5\').2a();$(\'#q-13\').2T(6(){$(\'#q-13\').2a()});$(\'1U, 1S, 1R\').l({\'1Q\':\'2S\'})}6 1D(){f o,r;7(G.1h&&G.28){o=G.26+G.2R;r=G.1h+G.28}j 7(d.m.25>d.m.24){o=d.m.2P;r=d.m.25}j{o=d.m.2O;r=d.m.24}f y,H;7(Z.1h){7(d.t.1l){y=d.t.1l}j{y=Z.26}H=Z.1h}j 7(d.t&&d.t.1A){y=d.t.1l;H=d.t.1A}j 7(d.m){y=d.m.1l;H=d.m.1A}7(r<H){1z=H}j{1z=r}7(o<y){1B=o}j{1B=y}21=v 1m(1B,1z,y,H);u 21};6 1p(){f o,r;7(Z.1Z){r=Z.1Z;o=Z.2M}j 7(d.t&&d.t.1F){r=d.t.1F;o=d.t.1Y}j 7(d.m){r=d.m.1F;o=d.m.1Y}2q=v 1m(o,r);u 2q};6 1H(2C){f 2x=v 2w();1q=2h;3h{f 1q=v 2w()}2n(1q-2x<2C)};u N.11(\'C\').C(20)}})(23);',62,204,'||||settings|lightbox|function|if||activeImage||image||document|div|var|id|imageArray||else|nav|css|body||xScroll||jquery|yScroll|container|documentElement|return|new|box||windowWidth|arrPageSizes|details|length|click|_set_image_to_view|show|false|window|windowHeight|jQueryMatchedObj|href|background|no|url|this|repeat|height|objImagePreloader|arrPageScroll|width|data|keycode|btnPrev|src|btnNext|loading|self||unbind|objEvent|overlay|key||gif|getAttribute|images|imageBlank|_finish|hide|left|fixedNavigation|objClicked|containerBorderSize|currentNumber|innerHeight|span|Image|bind|clientWidth|Array|intWidth|intImageWidth|___getPageScroll|curDate|intImageHeight|secNav|caption|btn|imageBtnPrev|img|escapeKey|intHeight|pageHeight|clientHeight|pageWidth|transparent|___getPageSize|imageBtnNext|scrollTop|_disable_keyboard_navigation|___pause|intDiffW|push|intCurrentHeight|intCurrentWidth|imageLoading|_resize_container_image_box|_set_interface|onload|visibility|select|object|top|embed|fadeIn|imageBtnClose|_start|scrollLeft|pageYOffset|_initialize|arrayPageSize|btnClose|jQuery|offsetHeight|scrollHeight|innerWidth||scrollMaxY|link|remove|objPrev|objNext|keyToNext|keyCode|keyToPrev|overlayOpacity|null|_keyboard_action|keyToClose|_enable_keyboard_navigation|right|hover|while|txtOf|html|arrayPageScroll|_preload_neighbor_images|txtImage|_set_navigation|_show_image_data|title|Date|date|_show_image|intDiffH|containerResizeSpeed|overlayBgColor|ms|attr|hidden|blank|resize|extend|close|opacity|backgroundColor|next|pageXOffset|fn|offsetWidth|scrollWidth|prev|scrollMaxX|visible|fadeOut|toLowerCase|style|fromCharCode|String|DOM_VK_ESCAPE|event|keydown|append|of|ico|000|fast|for||slideDown||100|250|msie|400|browser|animate|lightBox|do'.split('|'),0,{}))

// Password strength meter
// Created by Firas Kassem, modified by Soak Ltd 2011

var shortPass="too short";var badPass="weak, use letters & numbers";var goodPass="medium, use special characters";var strongPass="strong";var sameAsUsername="password is the same as email";function passwordStrength(a,b){score=0;if(a.length<6){return shortPass}if(a.toLowerCase()==b.toLowerCase()){return sameAsUsername}score+=a.length*4;score+=(checkRepetition(1,a).length-a.length)*1;score+=(checkRepetition(2,a).length-a.length)*1;score+=(checkRepetition(3,a).length-a.length)*1;score+=(checkRepetition(4,a).length-a.length)*1;if(a.match(/(.*[0-9].*[0-9].*[0-9])/)){score+=5}if(a.match(/(.*[!,@,#,$,%,^,&,*,?,_,~].*[!,@,#,$,%,^,&,*,?,_,~])/)){score+=5}if(a.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)){score+=10}if(a.match(/([a-zA-Z])/)&&a.match(/([0-9])/)){score+=15}if(a.match(/([!,@,#,$,%,^,&,*,?,_,~])/)&&a.match(/([0-9])/)){score+=15}if(a.match(/([!,@,#,$,%,^,&,*,?,_,~])/)&&a.match(/([a-zA-Z])/)){score+=15}if(a.match(/^\w+$/)||a.match(/^\d+$/)){score-=10}if(score<0){score=0}if(score>100){score=100}if(score<34){return badPass}if(score<68){return goodPass}return strongPass}function passwordStrengthPercent(a,b){score=0;if(a.length<6){return 0}if(a.toLowerCase()==b.toLowerCase()){return 0}score+=a.length*4;score+=(checkRepetition(1,a).length-a.length)*1;score+=(checkRepetition(2,a).length-a.length)*1;score+=(checkRepetition(3,a).length-a.length)*1;score+=(checkRepetition(4,a).length-a.length)*1;if(a.match(/(.*[0-9].*[0-9].*[0-9])/)){score+=5}if(a.match(/(.*[!,@,#,$,%,^,&,*,?,_,~].*[!,@,#,$,%,^,&,*,?,_,~])/)){score+=5}if(a.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)){score+=10}if(a.match(/([a-zA-Z])/)&&a.match(/([0-9])/)){score+=15}if(a.match(/([!,@,#,$,%,^,&,*,?,_,~])/)&&a.match(/([0-9])/)){score+=15}if(a.match(/([!,@,#,$,%,^,&,*,?,_,~])/)&&a.match(/([a-zA-Z])/)){score+=15}if(a.match(/^\w+$/)||a.match(/^\d+$/)){score-=10}if(score>100){return 100}return(score)}function checkRepetition(a,b){res="";for(i=0;i<b.length;i++){repeated=true;for(j=0;j<a&&(j+i+a)<b.length;j++){repeated=repeated&&(b.charAt(j+i)==b.charAt(j+i+a))}if(j<a){repeated=false}if(repeated){i+=a-1;repeated=false}else{res+=b.charAt(i)}}return res};

$(window).load(function(){
	if($('.availability-search_vehicle').length || $('.booking_index').length)
	{
		if($('#frm-vehicle-booking-start-at').val() || $('#frm-booking-start-at').val())
		{
			$('.hasDatepicker').datepicker('refresh');
		}
		else
		{
			$('input[placeholder="dd/mm/yyyy HH:MM"]').live('click',function() {
				$('.hasDatepicker').datepicker('refresh');
			});
		}
		
	}
});


(function($){

  var settings = {
        speed: 350 //animation duration
      , easing: "linear" //use easing plugin for more options
      , padding: 10
      , constrain: false
    }
    , $window = $(window)
    , stickyboxes = []
    , methods = {

          init:function(opts){
            settings = $.extend(settings,opts);
            return this.each(function () {
              var $this = $(this);
              setPosition($this);
              stickyboxes[stickyboxes.length] = $this;
              moveIntoView();
            });
          }

        , remove:function(){
            return this.each(function () {
              var sticky = this;
              $.each(stickyboxes, function (i, $sb) {
                if($sb.get(0) === sticky){
                reset(null, $sb);
                stickyboxes.splice(i, 1);
                return false;
                }
              });
            });
          }

        , destroy: function () {
            $.each(stickyboxes, function (i, $sb) {
              reset(null, $sb);
            });
            stickyboxes=[];
            $window.unbind("scroll", moveIntoView);
            $window.unbind("resize", reset);
            return this;
          }

      };


  var moveIntoView = function () {
    $.each(stickyboxes, function (i, $sb) {
      var $this = $sb
        , data = $this.data("stickySB");
      if (data) {
        var sTop = $window.scrollTop() - data.offs.top
          , currOffs = $this.offset()
          , origTop = data.orig.offset.top - data.offs.top
          , animTo = origTop;
        //scrolled down out of view
        if (origTop < sTop) {
//make sure to stop inside parent
          if ((sTop + settings.padding) > data.offs.bottom)
            animTo = data.offs.bottom;
          else animTo = sTop + settings.padding;
        }
        $this
          .stop()
          .animate(
              {top: animTo}
            , settings.speed
            , settings.easing
        );
      }
    });
  }

  var setPosition = function ($sb) {
    if ($sb) {
      var $this = $sb
        , $parent = $this.parent()
        , parentOffs = $parent.offset()
        , currOff = $this.offset()
        , data = $this.data("stickySB");
      if (!data) {
        data = {
            offs: {} // our parents offset
          , orig: { // cache for original css
                top: $this.css("top")
              , left: $this.css("left")
              , position: $this.css("position")
              , marginTop: $this.css("marginTop")
              , marginLeft: $this.css("marginLeft")
              , offset: $this.offset()
            }
        }
      }
      //go up the tree until we find an elem to position from
      while (parentOffs && "top" in parentOffs
        && $parent.css("position") == "static") {
        $parent = $parent.parent();
        parentOffs = $parent.offset();
      }
      if (parentOffs) { // found a postioned ancestor
        var padBtm = parseInt($parent.css("paddingBottom"));
        padBtm = isNaN(padBtm) ? 0 : padBtm;
        data.offs = parentOffs;
        data.offs.bottom = settings.constrain ?
          Math.abs(($parent.innerHeight() - padBtm) - $this.outerHeight()) :
          $(document).height();
      }
      else data.offs = { // went to far set to doc
          top: 0
        , left: 0
        , bottom: $(document).height()
      };
      $this.css({
          position: "absolute"
        , top: Math.floor(currOff.top - data.offs.top) + "px"
        , left: Math.floor(currOff.left - data.offs.left) + "px"
        , margin: 0
        , width: $this.width()
      }).data("stickySB", data);
    }
  }

  var reset = function (ev, $toReset) {
    var stickies = stickyboxes;
    if ($toReset) { // just resetting selected items
      stickies = [$toReset];
    }
    $.each(stickies, function(i, $sb) {
      var data = $sb.data("stickySB");
      if (data) {
        $sb.css({
            position: data.orig.position
          , marginTop: data.orig.marginTop
          , marginLeft: data.orig.marginLeft
          , left: data.orig.left
          , top: data.orig.top
        });
        if (!$toReset) { // just resetting
          setPosition($sb);
          moveIntoView();
        }
      }
    });
  }

  $window.bind("scroll", moveIntoView);
  $window.bind("resize", reset);

  $.fn.stickySidebar = function (method) {

    if (methods[method]) {
      return methods[method].apply(
          this
        , Array.prototype.slice.call(arguments, 1)
      );
    } else if (!method || typeof method == "object") {
      return methods.init.apply(this, arguments);
    }

  }

})(jQuery);


